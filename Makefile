JAVAC = javac
JAVAFLAGS = -O -d bin -encoding utf-8
CLASSPATH = lib/liblinear-1.33-with-deps.jar:lib/jwnl.jar:lib/commons-logging.jar:lib/jdom.jar:lib/trove.jar:lib/maxent-2.4.0.jar:lib/opennlp-tools-1.3.0.jar:lib/weka-3.2.3.jar:lib/libsvm.jar:lib/commons-lang3-3.4/commons-lang3-3.4.jar:lib/stanford-parser-3.6.0-models.jar:lib/stanford-parser.jar:lib/slf4j-api.jar:lib/slf4j-simple.jar

SENSEVAL2TRG = corpora/english-lex-sample/train/eng-lex-sample.training.xml corpora/english-lex-sample/train/eng-lex-sample.training.key
SENSEVAL2TEST = corpora/english-lex-sample/test/eng-lex-samp.evaluation.xml
SENSEVAL2_REORDERED_TRG = reordered-senseval/training_data.xml reordered-senseval/training_data.key
SENSEVAL2_REORDERED_TEST = reordered-senseval/test_data.xml

SENSEVAL3TRG = EnglishLS.train/EnglishLS.train.xml EnglishLS.train/EnglishLS.train.key
SENSEVAL3TEST = EnglishLS.test/EnglishLS.test.xml 

all: build
build:
	mkdir -p bin
	$(JAVAC) -classpath $(CLASSPATH) $(JAVAFLAGS) src/sg/edu/nus/comp/nlp/ims/*/*.java src/sg/edu/nus/comp/nlp/ims/*/*/*.java
	cd bin;	pwd; jar cvf ../ims-`date +%Y-%m-%d`.jar sg; cd ..
	cp ims-`date +%Y-%m-%d`.jar ims.jar
clean:
	@-rm resultDir/*
	@-rm trainedDir/*

senseval2: clean build
	
	./train_with_senna.bash $(SENSEVAL2TRG)  trainedDir
	./test_with_senna.bash trainedDir/ $(SENSEVAL2TEST) resultDir/ 
	cd utility; python combined_mul_keys.py;python change_first_token_to_subject.py ; cd ..;
	./scorer.bash resultDir/all.combined.amended.result answers+misc/tasks/english-lex-sample/key  

senseval3: clean build
	./train_with_senna.bash $(SENSEVAL3TRG)  trainedDir
	./test_with_senna.bash trainedDir/ $(SENSEVAL3TEST) resultDir/ 
	cd utility; python combined_mul_keys.py;python change_first_token_to_subject.py ; cd ..;
	./scorer.bash resultDir/all.combined.amended.result EnglishLS.test/EnglishLS.test.amended.key 

senseval2_ims: clean build
	
	./train_one.bash $(SENSEVAL2TRG)  trainedDir
	./test_one.bash trainedDir/ $(SENSEVAL2TEST) resultDir/ 
	cd utility; python combined_mul_keys.py;python change_first_token_to_subject.py ; cd ..;
	./scorer.bash resultDir/all.combined.amended.result answers+misc/tasks/english-lex-sample/key  

senseval3_ims: clean build
	./train_one.bash $(SENSEVAL3TRG)  trainedDir
	./test_one.bash trainedDir/ $(SENSEVAL3TEST) resultDir/ 
	cd utility; python combined_mul_keys.py;python change_first_token_to_subject.py ; cd ..;
	./scorer.bash resultDir/all.combined.amended.result EnglishLS.test/EnglishLS.test.amended.key 

senseval2_reordered: clean build
	./train_with_senna.bash $(SENSEVAL2_REORDERED_TRG)  trainedDir
	./test_with_senna.bash trainedDir/ $(SENSEVAL2_REORDERED_TEST) resultDir/ 
	cd utility; python combined_mul_keys.py;python change_first_token_to_subject.py ; cd ..;
	./scorer.bash resultDir/all.combined.amended.result reordered-senseval/test_data.key 

evaluate2_ls:
	cd utility; python combined_mul_keys.py;python change_first_token_to_subject.py ; cd ..;
	
	./scorer.bash resultDir/all.combined.amended.result answers+misc/tasks/english-lex-sample/key
        
evaluate3_ls:
	cd utility; python combined_mul_keys.py;python change_first_token_to_subject.py ; cd ..;
	./scorer.bash resultDir/all.combined.amended.result EnglishLS.test/EnglishLS.test.amended.key 


NOUN_TRG_FILES = onemillion/noun/training_data.xml onemillion/noun/training_data.key
train_one_million: build
	./train_with_senna.bash $(NOUN_TRG_FILES) allWordsTrainedDir


test_all_words_se2: build
	./testFine.bash oneMillionTrainedDir/ corpora/english-all-words/test/eng-all-words.test.xml  examples/se2.eng-all-words.test.lexelt SE2_AW_output examples/wn17.index.sense

