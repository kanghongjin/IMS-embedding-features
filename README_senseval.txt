2000  ./train_one.bash corpora/english-lex-sample/train/eng-lex-sample.training.xml corpora/english-lex-sample/train/eng-lex-sample.training.key trainedDir
 2001  history | grep test
  2002  ./test_one.bash trainedDir/ corpora/english-group-lex-sample/test/eng-lex-samp.evaluation.xml resultDir/
   2003  cd utility/
    2004  python combined_mul_keys.py 
     2005  python change_first_token_to_subject.py 
      2006  cd ..
       2007  ./scorer.bash 
        2008  ./scorer.bash resultDir/all.combined.amended.result answers+misc/tasks/english-lex-sample/key


the above are commands to replicate the experiments, but running the test can be done using ./evaluate_senseval*

on a side note, if getting an error about the platform, change the jdk you are using to oracle's. OpenJDk gives errors somehow.
java 8 should be used, this may require adding another repository to apt-get, so don't be alarmed if oracle's java 8 is not present
 by default in apt-get


 FilePaths.py should give a better understanding of some the various folders.
